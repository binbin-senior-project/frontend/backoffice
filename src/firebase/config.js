import * as firebase from "firebase/app";
import "firebase/auth";

const app = firebase.initializeApp({
  apiKey: "AIzaSyAv-gy5CivM_4FNVf0Wx2nc0VlRQ5gXyNo",
  authDomain: "binbin-backoffice.firebaseapp.com",
  databaseURL: "https://binbin-backoffice.firebaseio.com",
  projectId: "binbin-backoffice",
  storageBucket: "binbin-backoffice.appspot.com",
  messagingSenderId: "465370632429",
  appId: "1:465370632429:web:b9231c100e87f6e5627ce4",
  measurementId: "G-3TCP4LH871",
});

export default app;
