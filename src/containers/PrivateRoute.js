import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Route, Redirect, useLocation } from "react-router-dom";
import { Spin } from "antd";
import { loadTokenAction } from "../actions/auth";

const PrivateRoute = ({ isLoading, isLoggedIn, children, loadToken }) => {
  const location = useLocation();

  useEffect(() => {
    if (isLoading && !isLoggedIn) {
      loadToken();
    }
  }, []);

  if (isLoading) {
    return (
      <div
        style={{
          width: "100vw",
          height: "100vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Spin size="medium" />
      </div>
    );
  }

  return (
    <Route
      render={({ location }) =>
        isLoggedIn ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
  isLoggedIn: state.auth.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({
  loadToken: () => dispatch(loadTokenAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
