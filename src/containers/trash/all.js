import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Input, Table, Row, Col, Button } from "antd";
import GetTrashes from "./query/trashes";
import DeleteTrash from "./mutation/delete";

const { Search } = Input;

const All = ({ location, accessToken }) => {
  const path = location.pathname;
  const [offset, setOffset] = useState(0);
  const { getTrashes, trashes, isLoading } = GetTrashes(
    offset,
    100,
    accessToken
  );
  const { deleteTrash } = DeleteTrash(accessToken);

  useEffect(() => {
    getTrashes();
  }, [location]);

  const columns = [
    {
      title: "ชื่อประเภทขยะ",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link to={`/trashes/${record.id}/edit`}>{text}</Link>
      ),
    },
    {
      title: "รหัสโค้ด",
      dataIndex: "code",
      key: "code",
    },
    {
      title: "ราคาแต้มปกติ",
      dataIndex: "regularPoint",
      key: "regularPoint",
      render: (text, record) => <span>{text} แต้ม</span>,
    },
    {
      title: "ราคาแต้มขาย",
      dataIndex: "salePoint",
      key: "salePrice",
      render: (text, record) => <span>{text} แต้ม</span>,
    },
    {
      title: "แอคชัน",
      key: "action",
      render: (text, record) => (
        <span>
          <Link to={`/trashes/${record.id}/edit`}>แก้ไข</Link>
          <Button
            type="link"
            danger
            onClick={() => {
              deleteTrash({ variables: { input: { trashID: record.id } } });
              setTimeout(() => getTrashes(), 100);
            }}
          >
            ลบ
          </Button>
        </span>
      ),
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Row style={{ marginBottom: 20 }} align="middle" justify="space-between">
        <Col span={20}></Col>
        <Col>
          <Button type="primary" size="large">
            <Link to={`${path}/new`}>เพิ่มขยะใหม่</Link>
          </Button>
        </Col>
      </Row>

      <Row>
        <Col span={24}>
          <Table columns={columns} dataSource={trashes} loading={isLoading} />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(All);
