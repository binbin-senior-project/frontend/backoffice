import React from "react";
import { connect } from "react-redux";
import { Row, Col, Divider, Button, Form, Input, Select } from "antd";
import { H2 } from "../../components/global";
import CreateTrash from "./mutation/create";

const { Option } = Select;

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 19 },
};

const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not validate email!",
    number: "${label} is not a validate number!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const New = ({ accessToken }) => {
  const { createTrash, isLoading } = CreateTrash(accessToken);

  const onFinish = (values) => {
    createTrash({
      variables: {
        input: {
          name: values.name,
          code: values.code,
          regularPoint: values.regularPoint,
          salePoint: values.salePoint,
        },
      },
    });
  };

  return (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={onFinish}
      validateMessages={validateMessages}
    >
      <Row gutter={20}>
        <Col span={17}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>รายละเอียด</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "20px 0" }}>
              <Form.Item
                name="name"
                label="ชื่อขยะ"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="code"
                label="รหัสโค้ด"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="regularPoint"
                label="ราคาแต้มปกติ"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="salePoint"
                label="ราคาแต้มขาย"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
            </div>
          </div>
        </Col>
        <Col span={7}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>จัดการ</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "10px 20px" }}>
              <Row gutter={[20]} align="middle" justify="space-between">
                <Col>
                  <p style={{ margin: 0 }}>สถานะ</p>
                </Col>
                <Col span={17}>
                  <Select defaultValue="available" style={{ width: "100%" }}>
                    <Option value="available">ใช้งานได้</Option>
                    <Option value="unavilable">ปิดใช้งาน</Option>
                  </Select>
                </Col>
              </Row>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "0 20px 10px 20px" }}>
              <Button type="primary" size="large" block htmlType="submit">
                เพิ่มขยะใหม่
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </Form>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(New);
