import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const DeleteTrash = (accessToken) => {
  const [success, setSuccess] = useState({});
  const [deleteTrash, { loading }] = useMutation(
    gql`
      mutation DeleteTrash($input: DeleteTrashInput!) {
        deleteTrash(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.deleteTrash);
      },
    }
  );

  return { deleteTrash, isLoading: loading };
};

export default DeleteTrash;
