import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const CreateTrash = (accessToken) => {
  const history = useHistory();
  const [createTrash, { loading }] = useMutation(
    gql`
      mutation CreateBin($input: CreateTrashInput!) {
        createTrash(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: () => {
        history.push("/trashes");
      },
    }
  );

  return { createTrash, isLoading: loading };
};

export default CreateTrash;
