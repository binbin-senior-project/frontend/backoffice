import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const UpdateTrash = (accessToken) => {
  const history = useHistory();
  const [updateTrash] = useMutation(
    gql`
      mutation UpdateTrash($input: UpdateTrashInput!) {
        updateTrash(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: () => {
        history.push("/trashes");
      },
    }
  );

  return { updateTrash };
};

export default UpdateTrash;
