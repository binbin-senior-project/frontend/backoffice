import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetTrash = (trashID, accessToken) => {
  const [trash, setTrash] = useState([]);
  const { loading } = useQuery(
    gql`
      query GetTrash($trashID: String!) {
        getTrash(trashID: $trashID) {
          id
          name
          code
          regularPoint
          salePoint
        }
      }
    `,
    {
      variables: { trashID },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setTrash(data?.getTrash);
      },
    }
  );

  return { trash, isLoading: loading };
};

export default GetTrash;
