import { useState } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetTrashes = (offset, limit, accessToken) => {
  const [trashes, setTrashes] = useState([]);
  const [getTrashes, { loading }] = useLazyQuery(
    gql`
      query GetTrashes($offset: Int!, $limit: Int!) {
        getTrashes(offset: $offset, limit: $limit) {
          id
          name
          code
          regularPoint
          salePoint
        }
      }
    `,
    {
      variables: { offset, limit },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setTrashes(data?.getTrashes);
      },
    }
  );

  return { getTrashes, trashes, isLoading: loading };
};

export default GetTrashes;
