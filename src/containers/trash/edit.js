import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Row, Col, Divider, Button, Form, Input } from "antd";
import { useParams } from "react-router-dom";
import { H2 } from "../../components/global";
import GetTrash from "./query/trash";
import UpdateTrash from "./mutation/update";

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 19 },
};

const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not validate email!",
    number: "${label} is not a validate number!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const Edit = ({ accessToken }) => {
  let { trashID } = useParams();
  const [_status, _setStatus] = useState("");
  const { trash, isLoading } = GetTrash(trashID, accessToken);
  const { updateTrash } = UpdateTrash(accessToken);

  const onFinish = (values) => {
    updateTrash({
      variables: {
        input: {
          trashID: trashID,
          name: values.name,
          code: values.code,
          regularPoint: values.regularPoint,
          salePoint: values.salePoint,
        },
      },
    });
  };

  useEffect(() => {
    _setStatus(trash.status);
  }, [trash]);

  if (isLoading || trash.id == undefined) {
    return <p>Loading</p>;
  }

  const initialValues = {
    id: trash.id,
    name: trash.name,
    code: trash.code,
    regularPoint: trash.regularPoint,
    salePoint: trash.salePoint,
  };

  return (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={onFinish}
      validateMessages={validateMessages}
      initialValues={initialValues}
    >
      <Row gutter={20}>
        <Col span={17}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>รายละเอียด</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "20px 0" }}>
              <Form.Item
                name="name"
                label="ชื่อขยะ"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="code"
                label="รหัสโค้ด"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="regularPoint"
                label="ราคาแต้มปกติ"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="salePoint"
                label="ราคาแต้มขาย"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
            </div>
          </div>
        </Col>
        <Col span={7}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>จัดการ</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "0 20px 10px 20px" }}>
              <Button type="primary" size="large" htmlType="submit" block>
                อัปเดตถังขยะ
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </Form>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(Edit);
