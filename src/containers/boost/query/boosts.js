import React, { useState } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetRequestBoosts = (offset, limit, accessToken) => {
  const [requests, setRequests] = useState([]);
  const [getRequestBoosts, { loading }] = useLazyQuery(
    gql`
      query GetRequestBoosts($offset: Int!, $limit: Int!) {
        getRequestBoosts(offset: $offset, limit: $limit) {
          id
          couponName
          productName
        }
      }
    `,
    {
      variables: { offset, limit },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setRequests(data?.getRequestBoosts);
      },
    }
  );

  return { getRequestBoosts, requests, isLoading: loading };
};

export default GetRequestBoosts;
