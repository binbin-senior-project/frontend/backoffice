import React, { useState } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetCouponProducts = (offset, limit, accessToken) => {
  const [products, setProducts] = useState([]);
  const [getCouponProducts, { loading }] = useLazyQuery(
    gql`
      query GetCouponProducts($offset: Int!, $limit: Int!) {
        getCouponProducts(offset: $offset, limit: $limit) {
          id
          name
          price
          day
        }
      }
    `,
    {
      variables: { offset, limit },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setProducts(data?.getCouponProducts);
      },
    }
  );

  return { getCouponProducts, products, isLoading: loading };
};

export default GetCouponProducts;
