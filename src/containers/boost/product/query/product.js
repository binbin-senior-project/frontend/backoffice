import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetCouponProduct = (productID, accessToken) => {
  const [couponProduct, setCouponProduct] = useState([]);
  const { loading } = useQuery(
    gql`
      query GetCouponProduct($productID: String!) {
        getCouponProduct(productID: $productID) {
          id
          name
          price
          day
        }
      }
    `,
    {
      variables: { productID },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setCouponProduct(data?.getCouponProduct);
      },
    }
  );

  return { couponProduct, isLoading: loading };
};

export default GetCouponProduct;
