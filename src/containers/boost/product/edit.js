import React from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import { Row, Col, Divider, Button, Form, Input, Select } from "antd";
import { H2 } from "../../../components/global";
import GetCouponProduct from "./query/product";
import UpdateCouponProduct from "./mutation/update";

const { Option } = Select;

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 19 },
};

const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not validate email!",
    number: "${label} is not a validate number!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const New = ({ accessToken }) => {
  const { productID } = useParams();
  const { couponProduct, isLoading } = GetCouponProduct(productID, accessToken);
  const { updateCouponProduct } = UpdateCouponProduct(accessToken);

  const onFinish = (values) => {
    updateCouponProduct({
      variables: {
        input: {
          productID: productID,
          name: values.name,
          price: values.price,
          day: values.day,
        },
      },
    });
  };

  if (isLoading || couponProduct.id == undefined) {
    return <p>Loading</p>;
  }

  return (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={onFinish}
      validateMessages={validateMessages}
      initialValues={{
        name: couponProduct.name,
        day: couponProduct.day,
        price: couponProduct.price,
      }}
    >
      <Row gutter={20}>
        <Col span={17}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>รายละเอียด</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "20px 0" }}>
              <Form.Item
                name="name"
                label="ชื่อแพ็กเกจ"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item name="price" label="ราคา" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
              <Form.Item
                name="day"
                label="จำนวนวัน"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
            </div>
          </div>
        </Col>
        <Col span={7}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>จัดการ</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "0 20px 10px 20px" }}>
              <Button type="primary" size="large" block htmlType="submit">
                เพิ่มประเภทคูปองใหม่
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </Form>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(New);
