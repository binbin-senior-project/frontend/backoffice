import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Input, Table, Row, Col, Button } from "antd";
import DeleteCouponProduct from "./mutation/delete";
import GetCouponProducts from "./query/products";

const { Search } = Input;

const All = ({ location, accessToken }) => {
  const path = location.pathname;
  const [offset, setOffset] = useState(0);
  const { getCouponProducts, products, isLoading } = GetCouponProducts(
    offset,
    6,
    accessToken
  );
  const { deleteCouponProduct } = DeleteCouponProduct(accessToken);

  useEffect(() => {
    getCouponProducts();
  }, [location]);

  const columns = [
    {
      title: "ชื่อแพ็กเกจ",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link to={`/boosts/products/${record.id}/edit`}>{text}</Link>
      ),
    },
    {
      title: "ราคา",
      dataIndex: "price",
      key: "price",
      render: (text, record) => text,
    },
    {
      title: "จำนวนวัน",
      dataIndex: "day",
      key: "day",
      render: (text, record) => text,
    },
    {
      title: "แอคชัน",
      key: "action",
      render: (text, record) => (
        <span>
          <Button type="link" style={{ padding: 0 }}>
            แก้ไข
          </Button>
          <Button
            type="link"
            danger
            onClick={() => {
              deleteCouponProduct({
                variables: { input: { productID: record.id } },
              });

              setTimeout(() => {
                getCouponProducts();
              }, 200);
            }}
          >
            ลบ
          </Button>
        </span>
      ),
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Row style={{ marginBottom: 20 }} align="middle" justify="space-between">
        <Col span={15}></Col>
        <Col>
          <Button type="primary" size="large">
            <Link to={`${path}/new`}>เพิ่มแพ็กเกจใหม่</Link>
          </Button>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Table columns={columns} dataSource={products} loading={isLoading} />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(All);
