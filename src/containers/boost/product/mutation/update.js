import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const UpdateCouponProduct = (accessToken) => {
  const history = useHistory();
  const [updateCouponProduct, { loading }] = useMutation(
    gql`
      mutation UpdateCouponProduct($input: UpdateProductInput!) {
        updateCouponProduct(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: () => {
        history.push("/boosts/products");
      },
    }
  );

  return { updateCouponProduct, isLoading: loading };
};

export default UpdateCouponProduct;
