import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const DeleteCouponProduct = (accessToken) => {
  const [success, setSuccess] = useState({});
  const [deleteCouponProduct, { loading }] = useMutation(
    gql`
      mutation DeleteCouponProduct($input: DeleteProductInput!) {
        deleteCouponProduct(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.deleteCouponProduct);
      },
    }
  );

  return { deleteCouponProduct, isLoading: loading };
};

export default DeleteCouponProduct;
