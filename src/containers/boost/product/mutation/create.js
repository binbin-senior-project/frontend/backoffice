import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const CreateCouponProduct = (accessToken) => {
  const history = useHistory();
  const [createCouponProduct, { loading }] = useMutation(
    gql`
      mutation CreateCouponProduct($input: CreateProductInput!) {
        createCouponProduct(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: () => {
        history.push("/boosts/products");
      },
    }
  );

  return { createCouponProduct, isLoading: loading };
};

export default CreateCouponProduct;
