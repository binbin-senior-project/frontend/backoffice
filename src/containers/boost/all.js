import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Input, Table, Row, Col, Button } from "antd";
import GetRequestBoosts from "./query/boosts";
import ApproveBoost from "./mutation/approve";
import RejectBoost from "./mutation/reject";

const { Search } = Input;

const All = ({ location, accessToken }) => {
  const path = location.pathname;
  const [offset, setOffset] = useState(0);
  const { getRequestBoosts, requests, isLoading } = GetRequestBoosts(
    offset,
    6,
    accessToken
  );
  const { approveBoost } = ApproveBoost(accessToken);
  const { rejectBoost } = RejectBoost(accessToken);

  useEffect(() => {
    getRequestBoosts();
  }, [location]);

  const columns = [
    {
      title: "ชื่อคูปอง",
      dataIndex: "couponName",
      key: "couponName",
      render: (text, record) => text,
    },
    {
      title: "แพ็กเกจ",
      dataIndex: "productName",
      key: "productName",
    },
    {
      title: "แอคชัน",
      key: "action",
      render: (text, record) => (
        <span>
          {record.status === "active" ? (
            <Button
              type="link"
              danger
              style={{ padding: 0 }}
              onClick={() => {
                rejectBoost({
                  variables: { input: { boostID: record.id } },
                });
                setTimeout(() => getRequestBoosts(), 200);
              }}
            >
              ไม่อนุมัติ
            </Button>
          ) : (
            <Button
              type="link"
              style={{ padding: 0 }}
              onClick={() => {
                approveBoost({
                  variables: { input: { boostID: record.id } },
                });
                setTimeout(() => getRequestBoosts(), 200);
              }}
            >
              อนุมัติ
            </Button>
          )}
        </span>
      ),
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Row>
        <Col span={24}>
          <Table columns={columns} dataSource={requests} loading={isLoading} />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(All);
