import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const RejectBoost = (accessToken) => {
  const [success, setSuccess] = useState(false);
  const [rejectBoost, { loading }] = useMutation(
    gql`
      mutation RejectBoost($input: RejectBoostInput!) {
        rejectBoost(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.rejectBoost?.success);
      },
    }
  );

  return { rejectBoost, isLoading: loading, success };
};

export default RejectBoost;
