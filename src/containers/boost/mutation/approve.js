import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const ApproveBoost = (accessToken) => {
  const [success, setSuccess] = useState(false);
  const [approveBoost, { loading }] = useMutation(
    gql`
      mutation ApproveBoost($input: ApproveBoostInput!) {
        approveBoost(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.approveBoost?.success);
      },
    }
  );

  return { approveBoost, isLoading: loading, success };
};

export default ApproveBoost;
