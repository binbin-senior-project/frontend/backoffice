import { useState } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetStores = (offset, limit, accessToken) => {
  const [stores, setStores] = useState([]);
  const [getStores, { loading }] = useLazyQuery(
    gql`
      query GetStores($offset: Int!, $limit: Int!) {
        getStores(offset: $offset, limit: $limit) {
          id
          logo
          name
          tagline
          phone
          latitude
          longitude
          category {
            name
          }
          status
        }
      }
    `,
    {
      variables: { offset, limit },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setStores(data?.getStores);
      },
    }
  );

  return { getStores, stores, isLoading: loading };
};

export default GetStores;
