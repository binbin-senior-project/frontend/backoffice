import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetCoupons = (storeID, offset, limit, accessToken) => {
  const [coupons, setCoupons] = useState([]);
  const { loading } = useQuery(
    gql`
      query GetCouponsByStore($storeID: String!, $offset: Int!, $limit: Int!) {
        getCouponsByStore(storeID: $storeID, offset: $offset, limit: $limit) {
          id
          photo
          name
          description
          point
          condition
          expire
        }
      }
    `,
    {
      variables: { storeID, offset, limit },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setCoupons(data?.getCoupons);
      },
    }
  );

  return { coupons, isLoading: loading };
};

export default GetCoupons;
