import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetStore = (storeID, accessToken) => {
  const [store, setStore] = useState([]);
  const { loading } = useQuery(
    gql`
      query GetStore($storeID: String!) {
        getStore(storeID: $storeID) {
          id
          logo
          name
          tagline
          phone
          latitude
          longitude
          category {
            name
          }
          status
        }
      }
    `,
    {
      variables: { storeID },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setStore(data?.getStore);
      },
    }
  );

  return { store, isLoading: loading };
};

export default GetStore;
