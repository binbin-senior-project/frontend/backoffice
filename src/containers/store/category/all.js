import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Table, Row, Col, Button } from "antd";
import GetCategories from "./query/categories";
import DeleteCategory from "./mutation/delete";

const All = ({ location, accessToken }) => {
  const path = location.pathname;
  const [offset, setOffset] = useState(0);
  const { getCategories, categories, isLoading } = GetCategories(
    offset,
    6,
    accessToken
  );
  const { deleteCategory } = DeleteCategory(accessToken);

  useEffect(() => {
    getCategories();
  }, [location]);

  const columns = [
    {
      title: "ชื่อประเภทคูปอง",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link to={`/stores/categories/${record.id}/edit`}>{text}</Link>
      ),
    },
    {
      title: "แอคชัน",
      key: "action",
      render: (text, record) => (
        <span>
          <Link
            to={`/stores/categories/${record.id}/edit`}
            style={{ marginRight: 16 }}
          >
            แก้ไข
          </Link>
          <Button
            type="link"
            danger
            onClick={() => {
              deleteCategory({
                variables: { input: { categoryID: record.id } },
              });
              setTimeout(() => getCategories(), 200);
            }}
          >
            ลบ
          </Button>
        </span>
      ),
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Row style={{ marginBottom: 20 }} align="middle" justify="space-between">
        <Col span={15}></Col>
        <Col>
          <Button type="primary" size="large">
            <Link to={`${path}/new`}>เพิ่มประเภทร้านค้าใหม่</Link>
          </Button>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Table
            columns={columns}
            dataSource={categories}
            loading={isLoading}
          />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(All);
