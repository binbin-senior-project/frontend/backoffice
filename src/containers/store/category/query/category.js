import { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetStoreCategory = (categoryID, accessToken) => {
  const [category, setStoreCategory] = useState([]);
  const { loading } = useQuery(
    gql`
      query GetStoreCategory($categoryID: String!) {
        getStoreCategory(categoryID: $categoryID) {
          id
          name
        }
      }
    `,
    {
      variables: { categoryID },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      fetchPolicy: "no-cache",
      onCompleted: (data) => {
        setStoreCategory(data?.getStoreCategory);
      },
    }
  );

  return { category, isLoading: loading };
};

export default GetStoreCategory;
