import { useState } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetCategories = (offset, limit, accessToken) => {
  const [categories, setCategories] = useState([]);
  const [getCategories, { loading }] = useLazyQuery(
    gql`
      query GetStoreCategories($offset: Int!, $limit: Int!) {
        getStoreCategories(offset: $offset, limit: $limit) {
          id
          name
        }
      }
    `,
    {
      variables: { offset, limit },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      fetchPolicy: "no-cache",
      onCompleted: (data) => {
        setCategories(data?.getStoreCategories);
      },
    }
  );

  return { getCategories, categories, isLoading: loading };
};

export default GetCategories;
