import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const CreateCategory = (accessToken) => {
  const history = useHistory();
  const [createCategory, { loading }] = useMutation(
    gql`
      mutation CreateStoreCategory($input: CreateStoreCategoryInput!) {
        createStoreCategory(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: () => {
        history.push("/stores/categories");
      },
    }
  );

  return { createCategory, isLoading: loading };
};

export default CreateCategory;
