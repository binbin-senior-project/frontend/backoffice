import { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const DeleteCategory = (accessToken) => {
  const [success, setSuccess] = useState({});
  const [deleteCategory, { loading }] = useMutation(
    gql`
      mutation DeleteStoreCategory($input: DeleteStoreCategoryInput!) {
        deleteStoreCategory(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.deleteStoreCategory);
      },
    }
  );

  return { deleteCategory, isLoading: loading };
};

export default DeleteCategory;
