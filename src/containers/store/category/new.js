import React from "react";
import { connect } from "react-redux";
import { Row, Col, Divider, Button, Form, Input, Select } from "antd";
import { H2 } from "../../../components/global";
import Createcategory from "./mutation/create";

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 19 },
};

const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not validate email!",
    number: "${label} is not a validate number!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const New = ({ accessToken }) => {
  const { createCategory, isLoading } = Createcategory(accessToken);

  const onFinish = (values) => {
    createCategory({
      variables: {
        input: {
          name: values.name,
        },
      },
    });
  };

  return (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={onFinish}
      validateMessages={validateMessages}
    >
      <Row gutter={20}>
        <Col span={17}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>รายละเอียด</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "20px 0" }}>
              <Form.Item
                name="name"
                label="ประเภทร้านค้า"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
            </div>
          </div>
        </Col>
        <Col span={7}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>จัดการ</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "0 20px 10px 20px" }}>
              <Button type="primary" size="large" block htmlType="submit">
                เพิ่มประเภทร้านค้าใหม่
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </Form>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(New);
