import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const ActivateStore = (accessToken) => {
  const [success, setSuccess] = useState(false);
  const [activateStore, { loading }] = useMutation(
    gql`
      mutation ActivateStore($input: ActivateStoreInput!) {
        activateStore(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.activateStore);
      },
    }
  );

  return { activateStore, isLoading: loading };
};

export default ActivateStore;
