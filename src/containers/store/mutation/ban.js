import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const BanStore = (accessToken) => {
  const [success, setSuccess] = useState(false);
  const [banStore, { loading }] = useMutation(
    gql`
      mutation BanStore($input: BanStoreInput!) {
        banStore(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.banStore);
      },
    }
  );

  return { banStore, isLoading: loading };
};

export default BanStore;
