import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { Layout } from "antd";
import MyLayout from "../../components/layout";
const { Content } = Layout;

const Stores = ({ routes }) => {
  const { path } = useRouteMatch();

  return (
    <MyLayout title="จัดการร้านค้า">
      <Content style={{ margin: "20px 16px" }}>
        <Switch>
          {routes.map((route, i) => (
            <Route
              key={i}
              exact={i == 0}
              path={`${path}${route.path}`}
              render={props => (
                <route.component {...props} routes={route.routes} />
              )}
            />
          ))}
        </Switch>
      </Content>
    </MyLayout>
  );
};

export default Stores;
