import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Input, Table, Row, Col, Button, Tag, Avatar } from "antd";
import { UserOutlined } from "@ant-design/icons";
import GetStores from "./query/stores";
import BanStore from "./mutation/ban";
import ActivateStore from "./mutation/activate";

const { Search } = Input;

const All = ({ location, accessToken }) => {
  const [offset, setOffset] = useState(0);
  const { getStores, stores, isLoading } = GetStores(offset, 100, accessToken);
  const { banStore } = BanStore(accessToken);
  const { activateStore } = ActivateStore(accessToken);

  useEffect(() => {
    getStores();
  }, [location]);

  const columns = [
    {
      title: "โลโก้ร้านค้า",
      dataIndex: "logo",
      key: "logo",
      render: (text, record) => (
        <Avatar size={50} icon={<UserOutlined />} src={text} />
      ),
    },
    {
      title: "ชื่อร้านค้า",
      dataIndex: "name",
      key: "name",
      render: (text, record) => <Link to={`/stores/${record.id}`}>{text}</Link>,
    },
    {
      title: "แท็กไลน์",
      dataIndex: "tagline",
      key: "tagline",
    },
    {
      title: "เบอร์โทรร้าน",
      dataIndex: "phone",
      key: "phone",
      render: (text, record) => <a href="tel:022234545">{text}</a>,
    },
    {
      title: "สถานะ",
      dataIndex: "status",
      key: "status",
      render: (text, record) => {
        if (text === "active") {
          return <Tag color="blue">{text}</Tag>;
        }

        return <Tag color="red">{text}</Tag>;
      },
    },
    {
      title: "แอคชัน",
      key: "action",
      render: (text, record) => {
        if (record.status === "banned") {
          return (
            <Button
              type="link"
              style={{ padding: 0 }}
              onClick={() => {
                activateStore({ variables: { input: { storeID: record.id } } });
                setTimeout(() => getStores(), 200);
              }}
            >
              เปิดใช้งาน
            </Button>
          );
        }

        return (
          <Button
            type="link"
            danger
            style={{ padding: 0 }}
            onClick={() => {
              banStore({ variables: { input: { storeID: record.id } } });
              setTimeout(() => getStores(), 200);
            }}
          >
            ปิดบัญชี
          </Button>
        );
      },
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Row>
        <Col span={24}>
          <Table columns={columns} dataSource={stores} loading={isLoading} />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(All);
