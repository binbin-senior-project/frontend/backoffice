import React from "react";
import { connect } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { Descriptions, Row, Avatar, Tabs, Col, Table, Button } from "antd";
import { UserOutlined } from "@ant-design/icons";
import GetStore from "./query/store";
import GetCoupons from "./query/coupons";

const { TabPane } = Tabs;

const Show = ({ accessToken }) => {
  let { storeID } = useParams();
  const { store, isLoading } = GetStore(storeID, accessToken);
  const { coupons } = GetCoupons(storeID, 0, 6, accessToken);

  const columns = [
    {
      title: "ชื่อคูปอง",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link to={`/coupons/${record.id}`}>{text}</Link>
      ),
    },
    {
      title: "รายละเอียดคูปอง",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "แต้มราคา",
      dataIndex: "point",
      key: "point",
      render: (text, record) => <span>{text} แต้ม</span>,
    },
    {
      title: "แอคชัน",
      key: "action",
      render: (text, record) => (
        <span>
          {record.key === "2" ? (
            <Button type="link" danger style={{ padding: 0 }}>
              <Link to="#">ยกเลิกคูปอง</Link>
            </Button>
          ) : (
            <Button type="link" style={{ padding: 0 }}>
              <Link to="#">อนุมัติคูปอง</Link>
            </Button>
          )}
        </span>
      ),
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Descriptions title="ข้อมูลร้านค้า" bordered>
        <Descriptions.Item label="โลโก้ร้านค้า" span={3}>
          <Avatar size={50} icon={<UserOutlined />} src={store.logo} />
        </Descriptions.Item>
        <Descriptions.Item label="ชื่อร้านค้า" span={3}>
          {store.name}
        </Descriptions.Item>
        <Descriptions.Item label="แท็กไลน์" span={3}>
          {store.tagline}
        </Descriptions.Item>
        <Descriptions.Item label="เบอร์ติดต่อ" span={3}>
          {store.phone}
        </Descriptions.Item>
      </Descriptions>
      <Row style={{ marginTop: 20 }}>
        <Col span={24}>
          <Tabs type="card">
            <TabPane tab="คูปองของร้าน" key="1">
              <Row>
                <Col span={24}>
                  <Table columns={columns} dataSource={coupons} />
                </Col>
              </Row>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(Show);
