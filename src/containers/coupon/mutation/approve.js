import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const ApproveCoupon = (accessToken) => {
  const [success, setSuccess] = useState(false);
  const [approveCoupon, { loading }] = useMutation(
    gql`
      mutation ApproveCoupon($input: ApproveCouponInput!) {
        approveCoupon(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.approveCoupon?.success);
      },
    }
  );

  return { approveCoupon, isLoading: loading, success };
};

export default ApproveCoupon;
