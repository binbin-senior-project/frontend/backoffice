import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const RejectCoupon = (accessToken) => {
  const [success, setSuccess] = useState(false);
  const [rejectCoupon, { loading }] = useMutation(
    gql`
      mutation RejectCoupon($input: RejectCouponInput!) {
        rejectCoupon(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.rejectCoupon?.success);
      },
    }
  );

  return { rejectCoupon, isLoading: loading, success };
};

export default RejectCoupon;
