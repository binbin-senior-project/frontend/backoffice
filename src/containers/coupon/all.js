import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Tag, Avatar } from "antd";
import { Table, Row, Col, Button } from "antd";
import GetCoupons from "./query/coupons";
import ApproveCoupon from "./mutation/approve";
import RejectCoupon from "./mutation/reject";

const All = ({ location, accessToken }) => {
  const path = location.pathname;
  const [offset, setOffset] = useState(0);
  const { getCoupons, coupons, isLoading } = GetCoupons(
    offset,
    100,
    accessToken
  );
  const { approveCoupon } = ApproveCoupon(accessToken);
  const { rejectCoupon } = RejectCoupon(accessToken);

  useEffect(() => {
    getCoupons();
  }, [location]);

  const columns = [
    {
      title: "รูปโปรโมชัน",
      dataIndex: "photo",
      key: "photo",
      render: (text, record) => <Avatar shape="square" size={100} src={text} />,
    },
    {
      title: "ชื่อคูปอง",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link to={`/coupons/${record.id}`}>{text}</Link>
      ),
    },
    {
      title: "รายละเอียดคูปอง",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "แต้มราคา",
      dataIndex: "point",
      key: "point",
      render: (text, record) => <span>{text} แต้ม</span>,
    },
    {
      title: "สถานะ",
      dataIndex: "status",
      key: "status",
      render: (text, record) => {
        if (text === "active") {
          return <Tag color="blue">{text}</Tag>;
        }

        return <Tag>{text}</Tag>;
      },
    },
    {
      title: "แอคชัน",
      key: "action",
      render: (text, record) => (
        <span>
          {record.status === "active" ? (
            <Button
              type="link"
              danger
              style={{ padding: 0 }}
              onClick={() => {
                rejectCoupon({
                  variables: { input: { couponID: record.id } },
                });
                setTimeout(() => getCoupons(), 200);
              }}
            >
              ยกเลิกคูปอง
            </Button>
          ) : (
            <Button
              type="link"
              style={{ padding: 0 }}
              onClick={() => {
                approveCoupon({
                  variables: { input: { couponID: record.id } },
                });
                setTimeout(() => getCoupons(), 200);
              }}
            >
              อนุมัติคูปอง
            </Button>
          )}
        </span>
      ),
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Row>
        <Col span={24}>
          <Table columns={columns} dataSource={coupons} loading={isLoading} />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(All);
