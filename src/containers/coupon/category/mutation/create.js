import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const CreateCouponCategory = (accessToken) => {
  const history = useHistory();
  const [createCouponCategory, { loading }] = useMutation(
    gql`
      mutation CreateCouponCategory($input: CreateCouponCategoryInput!) {
        createCouponCategory(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: () => {
        history.push("/coupons/categories");
      },
    }
  );

  return { createCouponCategory, isLoading: loading };
};

export default CreateCouponCategory;
