import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const UpdateCategory = (accessToken) => {
  const history = useHistory();
  const [updateCategory] = useMutation(
    gql`
      mutation UpdateCouponCategory($input: UpdateCouponCategoryInput!) {
        updateCouponCategory(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: () => {
        history.push("/coupons/categories");
      },
    }
  );

  return { updateCategory };
};

export default UpdateCategory;
