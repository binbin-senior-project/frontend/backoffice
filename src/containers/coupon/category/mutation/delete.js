import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const DeleteCouponCategory = (accessToken) => {
  const [success, setSuccess] = useState({});
  const [deleteCouponCategory, { loading }] = useMutation(
    gql`
      mutation DeleteCouponCategory($input: DeleteCouponCategoryInput!) {
        deleteCouponCategory(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.deleteCouponCategory);
      },
    }
  );

  return { deleteCouponCategory, isLoading: loading };
};

export default DeleteCouponCategory;
