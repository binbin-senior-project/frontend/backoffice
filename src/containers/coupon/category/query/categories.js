import { useState } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetCouponCategories = (offset, limit, accessToken) => {
  const [categories, setCategories] = useState([]);
  const [getCategories, { loading }] = useLazyQuery(
    gql`
      query GetCouponCategories($offset: Int!, $limit: Int!) {
        getCouponCategories(offset: $offset, limit: $limit) {
          id
          name
        }
      }
    `,
    {
      variables: { offset, limit },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      fetchPolicy: "no-cache",
      onCompleted: (data) => {
        setCategories(data?.getCouponCategories);
      },
    }
  );

  return { getCategories, categories, isLoading: loading };
};

export default GetCouponCategories;
