import { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetCouponCategory = (categoryID, accessToken) => {
  const [category, setCategory] = useState({});
  const { loading } = useQuery(
    gql`
      query GetCouponCategory($categoryID: String!) {
        getCouponCategory(categoryID: $categoryID) {
          id
          name
        }
      }
    `,
    {
      variables: { categoryID },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setCategory(data?.getCouponCategory);
      },
    }
  );

  return { category, isLoading: loading };
};

export default GetCouponCategory;
