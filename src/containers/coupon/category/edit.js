import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Row, Col, Divider, Button, Form, Input } from "antd";
import { useParams } from "react-router-dom";
import { H2 } from "../../../components/global";
import GetCategory from "./query/category";
import UpdateCategory from "./mutation/update";

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 19 },
};

const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not validate email!",
    number: "${label} is not a validate number!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const Edit = ({ accessToken }) => {
  let { categoryID } = useParams();
  const { category, isLoading } = GetCategory(categoryID, accessToken);
  const { updateCategory } = UpdateCategory(accessToken);

  const onFinish = (values) => {
    updateCategory({
      variables: {
        input: {
          categoryID: categoryID,
          name: values.name,
        },
      },
    });
  };

  if (isLoading || category.id == undefined) {
    return <p>Loading</p>;
  }

  return (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={onFinish}
      validateMessages={validateMessages}
      initialValues={{
        name: category.name,
      }}
    >
      <Row gutter={20}>
        <Col span={17}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>รายละเอียด</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "20px 0" }}>
              <Form.Item
                name="name"
                label="ประเภทคูปอง"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
            </div>
          </div>
        </Col>
        <Col span={7}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>จัดการ</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "0 20px 10px 20px" }}>
              <Button type="primary" size="large" htmlType="submit" block>
                อัปเดตประเภทคูปอง
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </Form>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(Edit);
