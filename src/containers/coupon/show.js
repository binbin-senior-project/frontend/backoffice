import React from "react";
import { connect } from "react-redux";
import { Descriptions, Badge } from "antd";
import { useParams } from "react-router-dom";
import GetCoupon from "./query/coupon";

const Show = ({ accessToken }) => {
  let { couponID } = useParams();
  const { coupon, isLoading } = GetCoupon(couponID, accessToken);

  if (isLoading || coupon.id == undefined) {
    return <p>Loading</p>;
  }

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Descriptions title="ข้อมูลคูปอง" bordered>
        <Descriptions.Item label="รูปคูปอง" span={3}>
          <img alt={coupon.name} src={coupon.photo} width={200} />
        </Descriptions.Item>
        <Descriptions.Item label="ชื่อคูปอง" span={3}>
          {coupon.name}
        </Descriptions.Item>
        <Descriptions.Item label="รายละเอียด" span={3}>
          {coupon.description}
        </Descriptions.Item>
        <Descriptions.Item label="เงื่อนไข" span={3}>
          {coupon.condition}
        </Descriptions.Item>
        <Descriptions.Item label="แต้มราคา" span={3}>
          {coupon.point}
        </Descriptions.Item>
        <Descriptions.Item label="สถานะคูปอง" span={3}>
          <Badge status="processing" text="Running" />
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(Show);
