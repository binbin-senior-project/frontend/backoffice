import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetCoupon = (couponID, accessToken) => {
  const [coupon, setCoupon] = useState([]);
  const { loading } = useQuery(
    gql`
      query GetCoupon($couponID: String!) {
        getCoupon(couponID: $couponID) {
          id
          photo
          name
          point
          description
          condition
          expire
          status
        }
      }
    `,
    {
      variables: { couponID },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setCoupon(data?.getCoupon);
      },
    }
  );

  return { coupon, isLoading: loading };
};

export default GetCoupon;
