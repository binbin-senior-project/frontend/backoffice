import { useState } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetCoupons = (offset, limit, accessToken) => {
  const [coupons, setCoupons] = useState([]);
  const [getCoupons, { loading }] = useLazyQuery(
    gql`
      query GetCoupons($offset: Int!, $limit: Int!) {
        getCoupons(offset: $offset, limit: $limit) {
          id
          photo
          name
          point
          description
          condition
          expire
          status
        }
      }
    `,
    {
      variables: { offset, limit },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setCoupons(data?.getCoupons);
      },
    }
  );

  return { getCoupons, coupons, isLoading: loading };
};

export default GetCoupons;
