import React from "react";
import { connect } from "react-redux";
import MyLayout from "../../components/layout";
import { Layout, Row, Col, Statistic, Card } from "antd";
import { ArrowUpOutlined } from "@ant-design/icons";
import GetNewUserToday from "./query/newUser";
import GetNewStoreToday from "./query/newStore";
import GetNewCouponToday from "./query/newCoupon";
import GetNewTrashToday from "./query/newTrash";

const { Content } = Layout;

const Dashboard = ({ accessToken }) => {
  const { newUserToday } = GetNewUserToday(accessToken);
  const { newStoreToday } = GetNewStoreToday(accessToken);
  const { newCouponToday } = GetNewCouponToday(accessToken);
  const { newTrashToday } = GetNewTrashToday(accessToken);

  return (
    <MyLayout title="แดชบอร์ด">
      <Content style={{ margin: "20px 16px" }}>
        <Row gutter={[20, 20]}>
          <Col span={6}>
            <Card>
              <Statistic
                title="ยอดผู้ใช้ใหม่วันนี้"
                value={newUserToday}
                valueStyle={{ color: "#3f8600" }}
                prefix={<ArrowUpOutlined />}
              />
            </Card>
          </Col>
          <Col span={6}>
            <Card>
              <Statistic
                title="ยอดร้านค้าใหม่วันนี้"
                value={newStoreToday}
                valueStyle={{ color: "#3f8600" }}
                prefix={<ArrowUpOutlined />}
              />
            </Card>
          </Col>
          <Col span={6}>
            <Card>
              <Statistic
                title="ยอดคูปองใหม่วันนี้"
                value={newCouponToday}
                valueStyle={{ color: "#3f8600" }}
                prefix={<ArrowUpOutlined />}
              />
            </Card>
          </Col>
          <Col span={6}>
            <Card>
              <Statistic
                title="ยอดทิ้งขยะใหม่วันนี้"
                value={newTrashToday}
                valueStyle={{ color: "#3f8600" }}
                prefix={<ArrowUpOutlined />}
              />
            </Card>
          </Col>
        </Row>
      </Content>
    </MyLayout>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(Dashboard);
