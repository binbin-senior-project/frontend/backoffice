import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetNewTrashToday = (accessToken) => {
  const [newTrashToday, setNewTrashToday] = useState();
  const { loading } = useQuery(
    gql`
      query getNewTrashToday {
        getNewTrashToday
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setNewTrashToday(data?.getNewTrashToday);
      },
    }
  );

  return { newTrashToday, isLoading: loading };
};

export default GetNewTrashToday;
