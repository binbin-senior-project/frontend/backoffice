import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetNewCouponToday = (accessToken) => {
  const [newCouponToday, setNewCouponToday] = useState();
  const { loading } = useQuery(
    gql`
      query getNewCouponToday {
        getNewCouponToday
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setNewCouponToday(data?.getNewCouponToday);
      },
    }
  );

  return { newCouponToday, isLoading: loading };
};

export default GetNewCouponToday;
