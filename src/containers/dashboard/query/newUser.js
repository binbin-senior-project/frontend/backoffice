import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetNewUserToday = (accessToken) => {
  const [newUserToday, setNewUserToday] = useState();
  const { loading } = useQuery(
    gql`
      query GetNewUserToday {
        getNewUserToday
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setNewUserToday(data?.getNewUserToday);
      },
    }
  );

  return { newUserToday, isLoading: loading };
};

export default GetNewUserToday;
