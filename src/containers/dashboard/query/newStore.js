import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetNewStoreToday = (accessToken) => {
  const [newStoreToday, setNewStoreToday] = useState();
  const { loading } = useQuery(
    gql`
      query GetNewStoreToday {
        getNewStoreToday
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setNewStoreToday(data?.getNewStoreToday);
      },
    }
  );

  return { newStoreToday, isLoading: loading };
};

export default GetNewStoreToday;
