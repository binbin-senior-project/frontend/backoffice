import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Input, Table, Row, Col, Button } from "antd";
import { Tag } from "antd";
import GetBins from "./query/bins";
import DeleteBin from "./mutation/delete";

const { Search } = Input;

const All = ({ location, accessToken }) => {
  const path = location.pathname;
  const [offset, setOffset] = useState(0);
  const { bins, getBins, isLoading } = GetBins(offset, 100, accessToken);
  const { deleteBin } = DeleteBin(accessToken);

  useEffect(() => {
    getBins();
  }, [location]);

  const columns = [
    {
      title: "ชื่อถังขยะ",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link to={`/bins/${record.id}/edit`}>{text}</Link>
      ),
    },
    {
      title: "รายละเอียด",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "สถานะ",
      dataIndex: "status",
      key: "status",
      render: (text, record) => {
        if (text === "available") {
          return <Tag color="blue">{text}</Tag>;
        }

        if (text === "pending") {
          return <Tag>{text}</Tag>;
        }

        return <Tag color="red">{text}</Tag>;
      },
    },
    {
      title: "แอคชัน",
      key: "action",
      render: (text, record) => (
        <span>
          <Link to={`/bins/${record.id}/edit`} style={{ marginRight: 16 }}>
            แก้ไข
          </Link>
          <Button
            type="link"
            style={{ padding: 0 }}
            danger
            onClick={() => {
              deleteBin({ variables: { input: { binID: record.id } } });
              setTimeout(() => getBins(), 100);
            }}
          >
            ลบ
          </Button>
        </span>
      ),
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Row style={{ marginBottom: 20 }} align="middle" justify="space-between">
        <Col span={20}></Col>
        <Col>
          <Button type="primary" size="large">
            <Link to={`${path}/new`}>เพิ่มถังขยะใหม่</Link>
          </Button>
        </Col>
      </Row>

      <Row>
        <Col span={24}>
          <Table columns={columns} dataSource={bins} loading={isLoading} />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(All);
