import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetBin = (binID, accessToken) => {
  const [bin, setBin] = useState({});
  const { loading } = useQuery(
    gql`
      query GetBin($binID: String!) {
        getBin(binID: $binID) {
          id
          name
          description
          peripheral
          status
          latitude
          longitude
        }
      }
    `,
    {
      variables: { binID },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setBin(data?.getBin);
      },
    }
  );

  return { bin, isLoading: loading };
};

export default GetBin;
