import { useState } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetBins = (offset, limit, accessToken) => {
  const [bins, setBins] = useState([]);
  const [getBins, { loading }] = useLazyQuery(
    gql`
      query GetBins($offset: Int!, $limit: Int!) {
        getBins(offset: $offset, limit: $limit) {
          id
          name
          description
          peripheral
          status
          latitude
          longitude
        }
      }
    `,
    {
      variables: { offset, limit },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setBins(data?.getBins);
      },
    }
  );

  return { getBins, bins, isLoading: loading };
};

export default GetBins;
