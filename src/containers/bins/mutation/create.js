import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const CreateBin = (accessToken) => {
  const history = useHistory();
  const [createBin] = useMutation(
    gql`
      mutation CreateBin($input: CreateBinInput!) {
        createBin(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: () => {
        history.push("/bins");
      },
    }
  );

  return { createBin };
};

export default CreateBin;
