import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { useHistory } from "react-router-dom";

const UpdateBin = (accessToken) => {
  const history = useHistory();
  const [updateBin] = useMutation(
    gql`
      mutation UpdateBin($input: UpdateBinInput!) {
        updateBin(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: () => {
        history.push("/bins");
      },
    }
  );

  return { updateBin };
};

export default UpdateBin;
