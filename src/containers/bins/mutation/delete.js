import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const DeleteBin = (accessToken) => {
  const [success, setSuccess] = useState({});
  const [deleteBin, { loading }] = useMutation(
    gql`
      mutation DeleteBin($input: DeleteBinInput!) {
        deleteBin(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.deleteBin?.success);
      },
    }
  );

  return { deleteBin, isLoading: loading };
};

export default DeleteBin;
