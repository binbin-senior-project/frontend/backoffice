import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { Layout } from "antd";
import MyLayout from "../../components/layout";
import { H1 } from "../../components/global";

const { Header, Content } = Layout;

const Bins = ({ routes }) => {
  const { path } = useRouteMatch();

  return (
    <MyLayout title="จัดการถังขยะ">
      <Content style={{ margin: "20px 16px" }}>
        <Switch>
          {routes.map((route, i) => (
            <Route
              key={i}
              exact={i == 0}
              path={`${path}${route.path}`}
              render={(props) => (
                <route.component {...props} routes={route.routes} />
              )}
            />
          ))}
        </Switch>
      </Content>
    </MyLayout>
  );
};

export default Bins;
