import React, { useState } from "react";
import { connect } from "react-redux";
import { Row, Col, Divider, Button, Form, Input, Select } from "antd";
import { H2 } from "../../components/global";
import CreateBin from "./mutation/create";

const { Option } = Select;

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 19 },
};

const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not validate email!",
    number: "${label} is not a validate number!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const New = ({ accessToken }) => {
  const [_status, _setStatus] = useState("pending");
  const { createBin, isLoading } = CreateBin(accessToken);

  const onFinish = (values) => {
    createBin({
      variables: {
        input: {
          name: values.name,
          description: values.description,
          status: _status,
          peripheral: values.peripheral,
          latitude: values.latitude,
          longitude: values.longitude,
        },
      },
    });
  };

  return (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={onFinish}
      validateMessages={validateMessages}
    >
      <Row gutter={20}>
        <Col span={17}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>รายละเอียด</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "20px 0" }}>
              <Form.Item
                name="name"
                label="ชื่อถังขยะ"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="description"
                label="รายละเอียด"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="peripheral"
                label="เลขเครื่อง"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="latitude"
                label="ละติจูด"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="longitude"
                label="ลองจิจูด"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
            </div>
          </div>
        </Col>
        <Col span={7}>
          <div className="site-layout-background">
            <div style={{ padding: "10px 20px 0 20px" }}>
              <H2>จัดการ</H2>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "10px 20px" }}>
              <Row gutter={[20]} align="middle" justify="space-between">
                <Col>
                  <p style={{ margin: 0 }}>สถานะ</p>
                </Col>
                <Col span={17}>
                  <Select
                    defaultValue="available"
                    style={{ width: "100%" }}
                    onChange={(value) => _setStatus(value)}
                  >
                    <Option value="pending">กำลังติดตั้ง</Option>
                    <Option value="available">ใช้งานได้</Option>
                  </Select>
                </Col>
              </Row>
            </div>
            <Divider style={{ margin: "10px 0" }} />
            <div style={{ padding: "0 20px 10px 20px" }}>
              <Button type="primary" htmlType="submit" size="large" block>
                เพิ่มถังขยะใหม่
              </Button>
            </div>
          </div>
        </Col>
      </Row>
    </Form>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(New);
