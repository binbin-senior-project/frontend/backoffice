import React from "react";
import { Link } from "react-router-dom";
import { Button } from "antd";

export const columns = [
  {
    title: "ชื่อ",
    dataIndex: "firstname",
    key: "firstname",
    render: (text, record) => <Link to={`/users/${record.id}`}>{text}</Link>,
  },
  {
    title: "เบอร์ติดต่อ",
    dataIndex: "phone",
    key: "phone",
  },
  {
    title: "แต้มสะสมปัจจุบัน",
    dataIndex: "point",
    key: "point",
    sorter: {
      compare: (a, b) => a.point - b.point,
    },
  },
  {
    title: "แอคชัน",
    key: "action",
    render: (text, record) => (
      <Button type="link" danger style={{ padding: 0 }}>
        <Link to="#">ปิดบัญชี</Link>
      </Button>
    ),
  },
];
