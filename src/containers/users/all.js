import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Input, Table, Row, Col, Button } from "antd";
import { Tag } from "antd";
import GetUsers from "./query/users";
import BanUser from "./mutation/ban";
import ActivateUser from "./mutation/activate";

const { Search } = Input;

const All = ({ location, accessToken }) => {
  const [offset, setOffset] = useState(0);
  const { getUsers, users, isLoading } = GetUsers(offset, 100, accessToken);
  const { banUser } = BanUser(accessToken);
  const { activateUser } = ActivateUser(accessToken);

  useEffect(() => {
    getUsers();
  }, [location]);

  const columns = [
    {
      title: "ชื่อ",
      dataIndex: "firstname",
      key: "firstname",
      render: (text, record) => <Link to={`/users/${record.id}`}>{text}</Link>,
    },
    {
      title: "นามสกุล",
      dataIndex: "lastname",
      key: "lastname",
      render: (text, record) => text,
    },
    {
      title: "เบอร์ติดต่อ",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "แต้มสะสมปัจจุบัน",
      dataIndex: "point",
      key: "point",
      sorter: {
        compare: (a, b) => a.point - b.point,
      },
    },
    {
      title: "สถานะ",
      dataIndex: "status",
      key: "status",
      render: (text, record) => {
        if (text === "active") {
          return <Tag color="blue">{text}</Tag>;
        }

        return <Tag color="red">{text}</Tag>;
      },
    },
    {
      title: "แอคชัน",
      key: "action",
      render: (text, record) => {
        if (record.status === "banned") {
          return (
            <Button
              type="link"
              style={{ padding: 0 }}
              onClick={() => {
                activateUser({ variables: { input: { userID: record.id } } });
                setTimeout(() => getUsers(), 200);
              }}
            >
              เปิดใช้งาน
            </Button>
          );
        }

        return (
          <Button
            type="link"
            danger
            style={{ padding: 0 }}
            onClick={() => {
              banUser({ variables: { input: { userID: record.id } } });
              setTimeout(() => getUsers(), 200);
            }}
          >
            ปิดบัญชี
          </Button>
        );
      },
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Row>
        <Col span={24}>
          <Table columns={columns} dataSource={users} loading={isLoading} />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(All);
