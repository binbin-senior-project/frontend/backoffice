import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const BanUser = (accessToken) => {
  const [success, setSuccess] = useState(false);
  const [banUser, { loading }] = useMutation(
    gql`
      mutation BanUser($input: BanUserInput!) {
        banUser(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.banUser);
      },
    }
  );

  return { banUser, isLoading: loading };
};

export default BanUser;
