import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const ActivateUser = (accessToken) => {
  const [success, setSuccess] = useState(false);
  const [activateUser, { loading }] = useMutation(
    gql`
      mutation ActivateUser($input: ActivateUserInput!) {
        activateUser(input: $input) {
          success
        }
      }
    `,
    {
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setSuccess(data?.activateUser);
      },
    }
  );

  return { activateUser, isLoading: loading };
};

export default ActivateUser;
