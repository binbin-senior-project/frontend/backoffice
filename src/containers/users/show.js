import React, { useState } from "react";
import { connect } from "react-redux";
import { Descriptions, Row, Avatar, Tabs, Col, Table, Button } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { useParams } from "react-router-dom";
import GetUser from "./query/user";
import GetTransactions from "./query/transactions";

const { TabPane } = Tabs;

const Show = ({ accessToken }) => {
  const { userID } = useParams();
  const [offset, setOffet] = useState(0);
  const { user } = GetUser(userID, accessToken);
  const { transactions, isLoading } = GetTransactions(
    userID,
    offset,
    6,
    accessToken
  );

  const columns = [
    {
      title: "เลขรายการ",
      dataIndex: "id",
      key: "id",
      render: (text, record) => text,
    },
    {
      title: "ประเภทรายการ",
      dataIndex: "type",
      key: "type",
      render: (text, record) => text,
    },
    {
      title: "จำนวนแต้ม",
      dataIndex: "point",
      key: "point",
      render: (text, record) => text,
    },
    {
      title: "เมื่อ",
      dataIndex: "createdAt",
      key: "createdAt",
      render: (text, record) => text,
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 20, minHeight: 360 }}
    >
      <Descriptions title="ข้อมูลผู้ใช้งาน" bordered>
        <Descriptions.Item label="รูปภาพ" span={3}>
          <Avatar size={50} icon={<UserOutlined />} src={user.photo} />
        </Descriptions.Item>
        <Descriptions.Item label="ชื่อ-นามสกุล" span={3}>
          {`${user.firstname} ${user.lastname}`}
        </Descriptions.Item>
        <Descriptions.Item label="แต้มปัจจุบัน" span={3}>
          {user.point}
        </Descriptions.Item>
        <Descriptions.Item label="เบอร์ติดต่อ" span={3}>
          {user.phone}
        </Descriptions.Item>
      </Descriptions>
      <Row style={{ marginTop: 20 }}>
        <Col span={24}>
          <Tabs type="card">
            <TabPane tab="การทำรายการ" key="1">
              <Row>
                <Col span={24}>
                  <Table
                    columns={columns}
                    dataSource={transactions}
                    loading={isLoading}
                  />
                </Col>
              </Row>
            </TabPane>
          </Tabs>
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(Show);
