import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetTransactions = (userID, offset, limit, accessToken) => {
  const [transactions, setTransactions] = useState([]);
  const { loading } = useQuery(
    gql`
      query GetTransactionsByUser(
        $userID: String!
        $offset: Int!
        $limit: Int!
      ) {
        getTransactionsByUser(userID: $userID, offset: $offset, limit: $limit) {
          id
          type
          point
          description
          createdAt
        }
      }
    `,
    {
      variables: { userID, offset, limit },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setTransactions(data?.getTransactionsByUser);
      },
    }
  );

  return { transactions, isLoading: loading };
};

export default GetTransactions;
