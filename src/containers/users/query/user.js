import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetUser = (userID, accessToken) => {
  const [user, setUser] = useState([]);
  const { loading } = useQuery(
    gql`
      query GetUser($userID: String!) {
        getUser(userID: $userID) {
          id
          firstname
          lastname
          phone
          photo
          point
          status
        }
      }
    `,
    {
      variables: { userID },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setUser(data?.getUser);
      },
    }
  );

  return { user, isLoading: loading };
};

export default GetUser;
