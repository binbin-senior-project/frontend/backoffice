import { useState } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

const GetUsers = (offset, limit, accessToken) => {
  const [users, setUsers] = useState([]);
  const [getUsers, { loading }] = useLazyQuery(
    gql`
      query GetUsers($offset: Int!, $limit: Int!) {
        getUsers(offset: $offset, limit: $limit) {
          id
          firstname
          lastname
          phone
          photo
          point
          status
        }
      }
    `,
    {
      variables: { offset, limit },
      fetchPolicy: "no-cache",
      context: { headers: { authorization: `Bearer ${accessToken}` } },
      onCompleted: (data) => {
        setUsers(data?.getUsers);
      },
    }
  );

  return { getUsers, users, isLoading: loading };
};

export default GetUsers;
