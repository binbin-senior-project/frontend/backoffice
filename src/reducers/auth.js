import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_ERROR,
  USER_LOGOUT,
} from "../actions/auth";

const initialState = {
  accessToken: "",
  isLoading: true,
  isLoggedIn: false,
  error: { message: "" },
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case USER_LOGIN_REQUEST:
      return { ...state, isLoading: true };
    case USER_LOGIN_SUCCESS:
      return { ...state, ...action.data, isLoggedIn: true, isLoading: false };
    case USER_LOGIN_ERROR:
      return { ...initialState, error: action.error, isLoading: false };
    case USER_LOGOUT:
      return { ...initialState };
    default:
      return state;
  }
}

export default reducer;
