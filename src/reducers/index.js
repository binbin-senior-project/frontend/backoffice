import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import authReducer from "./auth";

const reducers = combineReducers({
  auth: authReducer,
});

export default createStore(reducers, applyMiddleware(thunk));
