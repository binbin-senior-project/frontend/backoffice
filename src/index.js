import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ApolloProvider } from "@apollo/react-hooks";
import { client } from "../src/graphql/config";
import store from "../src/reducers";
import Navigations from "./navigations";
import * as serviceWorker from "./serviceWorker";
import "firebase/auth";

ReactDOM.render(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <Navigations />
    </Provider>
  </ApolloProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
