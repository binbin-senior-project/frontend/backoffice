import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PrivateRoute from "../containers/PrivateRoute";
import routes from "./routes";
import "../css/app.css";

const Navigations = () => {
  return (
    <Router>
      <Switch>
        {routes.map((route, i) => {
          if (route.path == "/") {
            return (
              <Route
                key={i}
                path={route.path}
                exact
                render={(props) => (
                  <route.component {...props} routes={route.routes} exact />
                )}
              />
            );
          }

          return (
            <Route
              key={i}
              path={route.path}
              render={(props) => (
                <PrivateRoute>
                  <route.component {...props} routes={route.routes} />
                </PrivateRoute>
              )}
            />
          );
        })}
      </Switch>
    </Router>
  );
};

export default Navigations;
