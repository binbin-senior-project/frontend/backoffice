import Index from "../containers/index";
import Login from "../containers/login";
import Dashboard from "../containers/dashboard";

// Bins Pages
import Bins from "../containers/bins";
import Allbin from "../containers/bins/all";
import Newbin from "../containers/bins/new";
import Editbin from "../containers/bins/edit";

// Trashes Pages
import Trashes from "../containers/trash";
import Alltrash from "../containers/trash/all";
import Newtrash from "../containers/trash/new";
import Edittrash from "../containers/trash/edit";

// Coupons Pages
import Coupons from "../containers/coupon";
import Allcoupon from "../containers/coupon/all";
import Showcoupon from "../containers/coupon/show";
import AllCouponCategories from "../containers/coupon/category/all";
import NewCouponcategory from "../containers/coupon/category/new";
import EditCouponcategory from "../containers/coupon/category/edit";

// Boost Pages
import BoostCoupons from "../containers/boost";
import AllRequestBoostcoupon from "../containers/boost/all";
import AllCouponProducts from "../containers/boost/product/all";
import NewCouponProduct from "../containers/boost/product/new";
import EditCouponProduct from "../containers/boost/product/edit";

// Stores Pages
import Stores from "../containers/store";
import Allstore from "../containers/store/all";
import Showstore from "../containers/store/show";
import AllStoreCategories from "../containers/store/category/all";
import NewStoreCategory from "../containers/store/category/new";
import EditStoreCategory from "../containers/store/category/edit";

// Users Pages
import Users from "../containers/users";
import Alluser from "../containers/users/all";
import Showuser from "../containers/users/show";

const routes = [
  {
    path: "/",
    component: Login,
  },
  {
    path: "/dashboard",
    component: Dashboard,
  },
  {
    path: "/bins",
    component: Bins,
    routes: [
      {
        path: "/",
        component: Allbin,
      },
      {
        path: "/new",
        component: Newbin,
      },
      {
        path: "/:binID/edit",
        component: Editbin,
      },
    ],
  },
  {
    path: "/trashes",
    component: Trashes,
    routes: [
      {
        path: "/",
        component: Alltrash,
      },
      {
        path: "/new",
        component: Newtrash,
      },
      {
        path: "/:trashID/edit",
        component: Edittrash,
      },
    ],
  },
  {
    path: "/coupons",
    component: Coupons,
    routes: [
      {
        path: "/",
        component: Allcoupon,
      },
      {
        path: "/categories/new",
        component: NewCouponcategory,
      },
      {
        path: "/categories/:categoryID/edit",
        component: EditCouponcategory,
      },
      {
        path: "/categories",
        component: AllCouponCategories,
      },
      {
        path: "/:couponID",
        component: Showcoupon,
      },
    ],
  },
  {
    path: "/boosts",
    component: BoostCoupons,
    routes: [
      {
        path: "/",
        component: AllRequestBoostcoupon,
      },
      {
        path: "/products/new",
        component: NewCouponProduct,
      },
      {
        path: "/products/:productID/edit",
        component: EditCouponProduct,
      },
      {
        path: "/products",
        component: AllCouponProducts,
      },
    ],
  },
  {
    path: "/stores",
    component: Stores,
    routes: [
      {
        path: "/",
        component: Allstore,
      },
      {
        path: "/categories/new",
        component: NewStoreCategory,
      },
      {
        path: "/categories/:categoryID/edit",
        component: EditStoreCategory,
      },
      {
        path: "/categories",
        component: AllStoreCategories,
      },
      {
        path: "/:storeID",
        component: Showstore,
      },
    ],
  },
  {
    path: "/users",
    component: Users,
    routes: [
      {
        path: "/",
        component: Alluser,
      },
      {
        path: "/:userID",
        component: Showuser,
      },
    ],
  },
];

export default routes;
