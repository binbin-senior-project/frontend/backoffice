import firebase from "../firebase/config";

export const USER_LOGIN_REQUEST = "USER_LOGIN_REQUEST";
export const USER_LOGIN_SUCCESS = "USER_REQUEST_LOGIN_SUCCESS";
export const USER_LOGIN_ERROR = "USER_LOGIN_ERROR";
export const USER_LOGOUT = "USER_LOGOUT";

export const loadTokenAction = () => async (dispatch) => {
  dispatch({ type: USER_LOGIN_REQUEST });

  await firebase.auth().onAuthStateChanged(async (user) => {
    if (user) {
      const accessToken = await user.getIdToken();

      // Check Expire Token
      return dispatch({
        type: USER_LOGIN_SUCCESS,
        data: { accessToken },
      });
    }
    return dispatch({
      type: USER_LOGIN_ERROR,
      error: { message: "Something wrong!" },
    });
  });
};

export const loginAction = (email, password) => async (dispatch) => {
  await firebase.auth().signInWithEmailAndPassword(email, password);
  const user = await firebase.auth().currentUser;

  if (!user) {
    dispatch({
      type: USER_LOGIN_ERROR,
      error: { message: "Logged-In is failed" },
    });

    return;
  }

  const accessToken = await user?.getIdToken();

  dispatch({
    type: USER_LOGIN_SUCCESS,
    data: { accessToken },
  });
};

export const logoutAction = () => async (dispatch) => {
  firebase.auth().signOut();

  return dispatch({ type: USER_LOGOUT });
};
