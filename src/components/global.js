import styled from "styled-components";

export const MenuText = styled.span`
  font-family: "Sarabun", sans-serif;
  font-size: 14px;
  text-align: center;
  padding: 10px 0;
`;

export const H1 = styled.h1`
  font-family: "Sarabun", sans-serif;
  font-size: 22px;
  margin: 0;
`;

export const H2 = styled.h2`
  font-family: "Sarabun", sans-serif;
  font-size: 17px;
  margin: 0;
`;
