import React from "react";
import { Link, withRouter } from "react-router-dom";
import { Layout, Menu, Row, Col, Button } from "antd";
import {
  DashboardOutlined,
  RestOutlined,
  ExperimentOutlined,
  UserOutlined,
  ShopOutlined,
  ShakeOutlined,
  SoundOutlined,
} from "@ant-design/icons";
import { H1, H2, MenuText } from "./global";

const { Sider, Header, Footer } = Layout;
const { SubMenu } = Menu;

const MyLayout = ({ title, children, location }) => {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider>
        <div className="logo">
          <Link to="/">Binbin Backoffice</Link>
        </div>
        <Menu theme="dark" selectedKeys={[location.pathname]} mode="inline">
          <Menu.Item key="/dashboard">
            <DashboardOutlined />
            <Link to="/dashboard">
              <MenuText>แดชบอร์ด</MenuText>
            </Link>
          </Menu.Item>
          <SubMenu
            key="/bins"
            title={
              <span>
                <RestOutlined />
                <MenuText>จัดการถังขยะ</MenuText>
              </span>
            }
          >
            <Menu.Item key="/bins">
              <Link to="/bins">
                <MenuText>ถังขยะทั้งหมด</MenuText>
              </Link>
            </Menu.Item>
            <Menu.Item key="/bins/new">
              <Link to="/bins/new">
                <MenuText>เพิ่มถังขยะใหม่</MenuText>
              </Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="/trashes"
            title={
              <span>
                <ExperimentOutlined />
                <span>จัดการขยะ</span>
              </span>
            }
          >
            <Menu.Item key="/trashes">
              <Link to="/trashes">
                <MenuText>ขยะทั้งหมด</MenuText>
              </Link>
            </Menu.Item>
            <Menu.Item key="/trashes/new">
              <Link to="/trashes/new">
                <MenuText>เพิ่มขยะใหม่</MenuText>
              </Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="/coupons"
            title={
              <span>
                <ShakeOutlined />
                <span>จัดการคูปอง</span>
              </span>
            }
          >
            <Menu.Item key="/coupons">
              <Link to="/coupons">
                <MenuText>คูปองทั้งหมด</MenuText>
              </Link>
            </Menu.Item>
            <Menu.Item key="/coupons/categories">
              <Link to="/coupons/categories">
                <MenuText>หมวดหมู่คูปอง</MenuText>
              </Link>
            </Menu.Item>
            <Menu.Item key="/coupons/categories/new">
              <Link to="/coupons/categories/new">
                <MenuText>เพิ่มหมวดหมู่คูปอง</MenuText>
              </Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="/boosts"
            title={
              <span>
                <SoundOutlined />
                <span>จัดการบูสต์คูปอง</span>
              </span>
            }
          >
            <Menu.Item key="/boosts">
              <Link to="/boosts">
                <MenuText>คำขอบูสต์ทั้งหมด</MenuText>
              </Link>
            </Menu.Item>
            <Menu.Item key="/boosts/products">
              <Link to="/boosts/products">
                <MenuText>แพ็กเกจทั้งหมด</MenuText>
              </Link>
            </Menu.Item>
            <Menu.Item key="/boosts/products/new">
              <Link to="/boosts/products/new">
                <MenuText>เพิ่มแพ็กเกจ</MenuText>
              </Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="/users"
            title={
              <span>
                <UserOutlined />
                <span>จัดการผู้ใช้</span>
              </span>
            }
          >
            <Menu.Item key="/users">
              <Link to="/users">
                <MenuText>ผูู้ใช้ทั้งหมด</MenuText>
              </Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="/stores"
            title={
              <span>
                <ShopOutlined />
                <span>จัดการร้านค้า</span>
              </span>
            }
          >
            <Menu.Item key="/stores">
              <Link to="/stores">
                <MenuText>ร้านค้าทั้งหมด</MenuText>
              </Link>
            </Menu.Item>
            <Menu.Item key="/stores/categories">
              <Link to="/stores/categories">
                <MenuText>หมวดหมู่ร้านค้า</MenuText>
              </Link>
            </Menu.Item>
            <Menu.Item key="/stores/categories/new">
              <Link to="/stores/categories/new">
                <MenuText>เพิ่มหมวดหมู่ร้านค้า</MenuText>
              </Link>
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header
          className="site-layout-background"
          style={{ padding: "0 20px" }}
        >
          <Row justify="space-between">
            <Col>
              <H1>{title}</H1>
            </Col>
          </Row>
        </Header>
        {children}
        <Footer style={{ textAlign: "center" }}>
          <b>System:</b> Binbin Backoffice&nbsp;&nbsp;&nbsp;
          <b>Version:</b> 0.0.1&nbsp;&nbsp;&nbsp;
          <b>Environment:</b> Development
        </Footer>
      </Layout>
    </Layout>
  );
};

export default withRouter(MyLayout);
