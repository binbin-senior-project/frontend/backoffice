import ApolloClient from "apollo-boost";

export const client = new ApolloClient({
  // uri: "http://localhost:7003/query",
  uri: "http://backoffice.binbindev.com/query",
});
