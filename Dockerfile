FROM node:14 as build-deps

WORKDIR /usr/src/app

COPY package.json ./

RUN yarn

COPY . ./

RUN yarn build

# ----------------

FROM nginx:1.15-alpine

COPY --from=build-deps /usr/src/app/build /var/www

COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
